$(document).ready(function() {
    $('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });



    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $('.anchoring-icon').on('click', function() {
        $('.carousel').carousel(0);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.confrmation_bias-icon').on('click', function() {
        $('.carousel').carousel(1);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.in_group_bias-icon').on('click', function() {
        $('.carousel').carousel(2);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.negativity_bias-icon').on('click', function() {
        $('.carousel').carousel(3);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.optimism_bias-icon').on('click', function() {
        $('.carousel').carousel(4);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.pessimism_bias-icon').on('click', function() {
        $('.carousel').carousel(5);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.reactance-icon').on('click', function() {
        $('.carousel').carousel(6);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.the_barnum_effect-icon').on('click', function() {
        $('.carousel').carousel(7);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.the_framing_effect-icon').on('click', function() {
        $('.carousel').carousel(8);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.the_hallo_effect-icon').on('click', function() {
        $('.carousel').carousel(9);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.the_placebo_effect-icon').on('click', function() {
        $('.carousel').carousel(10);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
    $('.the_spotlight_effect-icon').on('click', function() {
        $('.carousel').carousel(11);
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    })
});
